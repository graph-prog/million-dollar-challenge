## Sample Query 1: Validate Guidelines

**Use Case:** To guarantee proper data anonymization and protection, constant response time on user events and no inconsistencies in asynchronous data handling.

**Problem:** Plain Java or C++ does not provide a facility to automatically validate the presence or absence of derived properties. There are external tools for static analysis and abstract interpretation, but the results are not attached to the graph nor accessible by querying the program code. This is especially a problem if new team members do not know how to reproduce the decisions that resulted in the present program code.

**Solution:** The properties derived by static analysis and abstract interpretation are attached to the graph and can therefore be accessed with graph queries. Then it is possible to write rules using normal programming language and query API (or just definitions which are translated to the actual rules) and also attach them to the graph. Extra tags attached to the graph (as specified in the models or by query matching) indicate where the rules should apply. If something looks like the wanted pattern, its properties are now specified.

**Sample Query:** "Install" and "Run" SampleQuery_1_ValidateGuidelines()


## Sample Query 2: Show StoredView

**Use Case:** To focus on only one aspect of the project, such as arithmetic, data handling or server communication domains.

**Problem:** With Java and C++ the presentation for viewing and editing the program code stays always roughly the same and can not be changed, even if you want to focus on specific implementation details (which can be spread throughout the project) or high-level architectural structures. You have to gather this information manually by reading and understanding the entire codebase through the appropriate glasses.

**Solution:** Since the information contained in the database needs to be translated into a readable format anyway, you can easily choose the projection used. This projection can be bijective (changes can be translated back) or otherwise can add the changes as automatically checkable requirements (a bijective projection then has to be edited until the check passes).

**Sample Query:** "Install" and "Run" SampleQuery_2_ShowStoredView()


## Sample Query 3: Apply TransformRecipe (Reformulate)

**Use Case:** To reformulate implementation and provide multiple implementation variants of which each is optimized for a different scenario.

**Problem:** Plain Java or C++ does not provide a facility to manage different variants of the same section in the program code. We are also not aware of any tool that could do this.

**Solution:** With the database, you can manage multiple variants of the same section in the program code. They also can be parameterizable or contain nested levels of variation. Then you can compare the properties of selected constellations of variation. Together with the last use case (Deriving Models), you can work on the program code from an abstract perspective. This means that you can e.g. require the program code to be parallelizable on many cores and the programming system will figure out an appropriate algorithm.

**Sample Query:** "Install" and "Run" SampleQuery_3_ApplyTransformRecipe_Reformulate()


## Sample Query 4: Apply TransformRecipe (Expand)

**Use Case:** To expand certain sections with orthogonal functionality and make the actually used selection of functionalities customizable by the user.

**Problem:** Plain Java or C++ does not provide a facility to automatically implement orthogonal functionality. There are external tools for program code transformation, but it is difficult to keep project code base, matching pattern and replacement code in the same programming language. This will make it more difficult to perform the other use-cases (such as Guideline Validation).

**Solution:** Since all necessary information is contained in the database, there is actually no difference between project code base, matching pattern and replacement code. This makes it very easy to chain together multiple transformation steps. Therefore, the replacement code can also be parameterized (even accepting program code as parameter) or even built by a program itself. It is now possible to build a modular program code, where similar structures of the program code are abstracted and unified. As long as enough of the program code is assembled from parameterizable "building blocks", the selection of functionalities can be customized from a high point of view.

A functionality is called orthogonal, if it has to be implemented at all points in the program code, where it comes into play. Imagine the feature to encrypt sensitive data, the feature to apply markup to user inputs or the feature to undo processing steps. While only some orthogonal features can simply be "grafted on", it is always possible to require them via Guidelines. Since Guidelines are already orthogonal (they are applied to every point in the program code), both tools complement each other perfectly. The next step is to automatically derive a Transformation Recipe from a Guideline, but this topic is very advanced and is only partially covered by this presentation (see next use case).

**Sample Query:** "Install" and "Run" SampleQuery_4_ApplyTransformRecipe_Expand()


## Sample Query 5: Apply TransformRecipe (Fix, automatically derived)

**Use Case:** To automatically correct inconsistencies in asynchronous data handling (auto-apply guideline).

**Problem:** Plain Java or C++ does not provide a facility to automatically correct violations of the specified Guidelines. We are also not aware of any tool that could do this.

**Solution:** To automatically transform a faulty program code into one without errors, multiple steps must be performed. Firstly, we use Guidelines to detect all violations in the program code. Second, we need to understand the code at these points and understand exactly what aspects of the present implementation are violating the Guideline. We use "abstract interpretation" for this. Third, we have to remove the error-causing aspects from the implemetation by finding another implementation that keeps the other aspects. Then we have to communicate our solution by telling the human programmer what of the functionality will be left after removing the violating aspects. Finally the human programmer has to confirm the preferred action.

**Sample Query:** "Install" and "Run" SampleQuery_5_ApplyTransformRecipe_Fix()


## Sample Query 6: Perform Analysis (Highlight Nuances)

**Use Case:** To query by features and properties and highlight the effects of nuances.

**Problem:** -

**Solution:** -

**Sample Query:** "Install" and "Run" SampleQuery_6_DeriveAnalysisResults_Highlight()


## Sample Query 7: Perform Analysis (Enforce Guidelines)

**Use Case:** To derive properties and connections to enforce guidelines and design decisions.

**Problem:** Plain Java or C++ does not provide a facility to apply the decisions made in the past to newly added program code. There are external tools for documenting design decisions, but these documents have to be manually applied to new program code. This causes the actual decision to get lost, while only its effects at the time remain.

**Solution:** I think I accidentally duplicated the first use case here, but have no time left to change it. So, I will describe the parts that work together for Guideline Validation.

The query API is a "fluent interface" internal DSL:
* for "query matching", to make a selection of AST nodes or other document's nodes and
* to define "predicates", that can be tested; Predicates can in turn be used to filter nodes in a query.
* to navigate in the graph and to mutate (transform) it.

The template mechanism can be used:
* to generate code (easier than with query API),
* to find similar structures in the code and
* to validate conformity of a section to the template

Abstract interpretation basically runs the program with all possible inputs at once, trying to figure out all possible behaviors at once. The results are then certainly true, certainly false or not possible to decide. It can propose modifications to make the results decidable.

Abstract interpretation can make statements e.g. about whether a section contains loops, whether their iteration count is certainly limited, whether there are indirect data-accesses, what instructions on the machine are used, which overflows could occur, where the code works on strings, how the data model is, what variables leak information to another variable or which of them have effect on the control flow, whether there is actually a "interpreter" state machine in an algorithm ..........

The most basic of these properties are used to understand the structure, the "what" of the code. The more detailed properties are then used to exactly specify the "how" of the present implementation. This technique can be used to analyze algorithms, as well as other types of code and even architectural patterns.

**Sample Query:** "Install" and "Run" SampleQuery_7_DeriveAnalysisResults_Enforce()


## Sample Query 8: Perform Analysis (Derive Models)

**Use Case:** To derive models to automatically build an understanding of the project.

**Problem:** Plain Java or C++ does not provide a facility to derive models. There are external tools for model driven development, but they are mostly one-way. This means, that you can define models to generate code, but as soon as you need to edit the generated code, these edits will not survive a change of the model. Either your model is from the beginning complete enough to cover all tasks, or you will in consequence stop using models at some point in development.

**Solution:** In the graph database you can mix sections of program code in different program languages. We use "local parsing" to make this possible. You can also embed a section in language X into another in language Y, as syntaxes are only the presentation (Although you have to choose one single runtime environment). There is also no problem to use multiple languages for code generation or just sections of program code within model definitions. Furthermore there will be a facility to use abstract interpretation to automatically derive very detailed models from program code, including the nuances of implementation that result in the presence or absence of specific properties. This is essential for allowing the computer to "understand" (in terms of machine-processable information) the project and to let the programmers work from an abstract perspective on the program code.

**Sample Query:** "Install" and "Run" SampleQuery_8_DeriveAnalysisResults_DeriveModels()

