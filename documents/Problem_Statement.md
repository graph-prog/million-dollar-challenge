# Problem Statement


Why do we want new tools to make better software at lower costs?

##

**THE PROBLEM EXISTS**: The status quo is that **(1)** creating software is expensive and **(2)** software has bugs that cause further costs.

Where:

**(1)** "creating software is expensive" means that the effort for implementing a working software is multiple times higher than specifying its requirements. This benchmark basically compares software development with other crafts like house building and one can refer to objective numbers in the news. These numbers also set a framework for how much money can be saved at most.

* The other interpretation of the word "expensive" is that there is actual potential for costs savings. We try to argue: "If it were easier to do X, we would save Y percent of the costs" and then "We *can* make this easier". This will remain hypothetical for now, but we have strong indications to assume this. See: [Use cases with queries](documents/Use_Cases_with_Queries.md).

* Then you can also interpret "potential to save costs" as the possibility to get more functionality (or better properties) for the same money. In contrast, today you either get too little software or pay too much.

**(2)** "software has bugs" means that even after creating the software, further costs will arise. These are costs for business standstills, productivity losses from complicated handling, costs of repairing the software and potentially costs of physical destruction.

* Both types of costs are actually two sides of the same coin. You could make creation more expensive to reduce operational costs or vice versa. We distinguish these types, because both actually are present in almost all software projects.


### Examples for expensive software projects (where some costs may not have been caused by software):

* 12.4 bn. GBP up from estimated 2.3 bn. GBP for UK NHS Connecting for Health [1]
* 1.7 bn. USD up from estimated 94 m. USD for US HealthCare.gov [2]
* 940 m. USD for US National Grid ERP in 2012 [3]

### Examples for software bugs causing high costs (of which some are not completely relevant, because actually formal methods were used):

* 20 bn. USD negative impact to economy because of Boeing 737 MAX groundings after crashes in 2018 and 2019 [4]
* 400 m. USD for Knight Capital trading loss in 2012 [5]
* 290 m. Euro for Ariane V88 in 1996 [6]


##


**THE PROBLEM IS "GRAPHY"**: The problem not only exists, but it is also inherently only solvable by graphs:

The graph is the "natural" format of program code, as it can:

1. be better processed by the computer,
2. be enriched with any information,
3. link components and describe relationships.

In contrast, text is not sufficient to handle large, feature-rich and internally complex software. We claim that the limitations of the text format are the main reason for the problem of expensive software. An important factor is, that the text format is not easily accessible for the computer, which is why many tasks are still not automated and would have be performed manually (or are neglected).

Although they **DO** bring benefits, none of the available tools will solve the problem completely. This covers all available software development tools and strategies such as: model driven development tools, static code analysis tools, formal methods, new programming languages, new libraries and frameworks, new source code editors, new (agile) development processes.

Because none of these text-based tools did solve the problem completely, we are convinced that this problem is inherently only solvable by graphs.

The mechanism behind the problem of rising costs is that the requirements are increasing, but the tools did not move away from text format. We acknowledge the progress of software development techniques, especially over the last few decades. But still we claim that it is not enough. Not for now and certainly not for the future.

Although we got used to limitations and high costs of software, as the complexity grows exponentially with the number of requirements, we forecast a hard limit within the next 100 years, where the problems effectively prevent further progress of software development techniques. The costs will then go through the roof, as the stagnant global economy cannot keep up with the growth of the population.

Therefore, we are convinced that this problem is getting worse.


##

[1] https://en.wikipedia.org/wiki/NHS_Connecting_for_Health

[2] https://en.wikipedia.org/wiki/HealthCare.gov

[3] https://www.businessinsider.com/national-grid-sap-1-billion-upgrade-cost-2014-10

[4] https://en.wikipedia.org/wiki/Financial_impact_of_the_Boeing_737_MAX_groundings

[5] https://en.wikipedia.org/wiki/Knight_Capital_Group

[6] https://de.wikipedia.org/wiki/Ariane_V88
