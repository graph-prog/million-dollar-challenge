We estimate that the following numbers will be reached within 10 years after market entry:


# Number of Beneficiaries

There live more than **680 m. people** in the US and Europe between the ages of 15 and 64. [1] [2] [3]

Assuming **10%** of the citizen of the US and Europe are demanding goods and services of which in turn **10%** can (by using this technology) be offered at a significantly lower price.

Assuming both percentages are distributed uniformly, then **1%** of the population or at least **6.8 m. people** would benefit from this technology.

This calculation assumes, that significantly lower supply costs are at least partially forwarded to the customer but ignores the fact that this would also rise the pricing pressure to the other 90% of the suppliers, effectively increasing adoption of our technology.

We estimate also at least 0.5% of Asia, Africa and Latin America would benefit from the productivity gain which are at least **33 m. people**. [4]


# Impact in USD

The annual gross domestic product of the US and Europe in 2022 will be **43 tn. USD**. [5] [6] This is only a number and kind of a definition how much 1 USD is worth relative to useful goods and services.

Assuming **10%** of the economy of the US and Europe would increase its productivity by **10%** by using this technology.

Then the annual gross domestic product of the US and Europe would increase by **430 bn. USD**.

Additionally, we estimate the productivity of **1%** of the economy of the US and Europe instead to double, wich would be an total increase by roughly **820 bn. USD**. The annual productivity gain could be reinvested, causing the productivity gain to grow exponentially.

This calculation makes no statement about the impact to other regions, which would add to this number.


# Size of the database

Todays software projects easily reach **10 m. lines of code**. [7]

We assume that the average line of code compiles to at least **3** AST nodes.

The history of all changes is stored in the database, which soon multiplies the number of nodes by **3**.

Any section of the program code can occur in multiple variants. These are suitable to customize a software for different buyers, to provide algorithms with configurable properties or to compare the properties of different implementations to make design decisions. We assume the number of nodes to be **doubled** in the average case.

The database also contains the properties which were derived by static analysis. We assume the number of nodes (without history) to be **doubled** in the average case.

The software is recording data during execution to measure how the users interact with the software. The users can also make suggestions within the software, which will also attached to the appropriate section of the program code. We assume the number of nodes (without history) to be **doubled** in the average case.

Then the database would contain at least **300 m. vertices + 300 m. edges**.

In fact, these are only the directly addressable (indexed) vertices. If we would count the JSON documents stored in each vertex, these numbers easily would multiply tenfold.

We assume that todays software projects easily reach **1000 requirements**. This includes decisions made during the development process.

We assume that at least **50%** of them can be validated automatically by comparing the properties which were derived by static analysis whith the specified targets. We call these special requirements then "Guidelines" and automatically validate the complete program code to satisfy them at every single point. With this automatic validation, the decisions are "enforced" and even new team members do not have to repeat the process of decision making nor to notice when they make a decision at all.

We assume that the average Guideline is related to at least **1%** of the project.

Then the database would contain at least **1.8 bn. edges**.


# Number of functional features

We are planning to provide a comprehensive collection of built-in:

* Easy-to-use templates for commonly used ArchitectureDefs and Models to accelerate the adoption of a more formal approach to software development

* Guidelines for commonly accepted good practices to improve code quality and software stability, considering all available information including derived properties

* TransformRecipes to automatically reformulate algorithms to optimize them for specified criteria

* StoredViews to highlight specific properties of the implementation, to derive models and to work on the project from this perspective, effectively adding changes to the viewed projection as a requirement to the actual program code, where it cannot be satisfied automatically.

Many of them are also parameterizable or are provided in multiple variants. If we interpret each of them as a separate feature, we easily reach a number of **10 000 functional features**.


# Adoption

We are planning to integrate our product with the most popular software development tools. Additionally, we will provide a generic interface to all other software development tools on the basis of text files. This will keep established build chains intact and let the customers decide to which degree they want to switch to our technology.

Therefore, the "ease of putting the solution into real-world use" will be maximized.

Taken literally, the "number and size of industries that could adopt this" is the entire software-producing industry, as long as they write program code to produce software. If we interpret it instead as "fraction of the economy, that could use software built with this technology", we get close to **100%** of the global economy, as long as they use software at all.


# References

[1] https://en.wikipedia.org/wiki/Demographics_of_the_United_States

[2] https://en.wikipedia.org/wiki/Europe

[3] https://www.statista.com/statistics/253408/age-distribution-in-the-european-union-eu/

[4] https://en.wikipedia.org/wiki/World_population

[5] https://en.wikipedia.org/wiki/Economy_of_the_United_States

[6] https://en.wikipedia.org/wiki/Economy_of_the_European_Union

[7] https://de.wikipedia.org/wiki/Lines_of_Code