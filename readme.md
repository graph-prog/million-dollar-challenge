# Submission for "Graph for All Million Dollar Challenge":
# Graph-Based Programming System / Database For Program Code

A collection of advanced software development tools that behave like well-known source code editors to create better software at lower costs, to make the economy more productive and to make products cheaper.

## Inspiration

Since I started programming, I have tried to work as error-free as possible and try to bring all criteria into harmony. Unfortunately, this is very expensive, inefficient and slow. My dream is to have tools that make this task easier for me.

An even bigger dream is that others will also use these tools to create → better software at → lower costs. This should make the economy → more productive, → products cheaper and → create wealth. This increase in productivity not only affects the IT industry, but ideally the entire global economy.

## What it does

The Graph-Based Programming System is a database for program code that provides advanced development tools but behaves and looks more or less like a normal source code editor.

Background: The graph format is a more natural format for program code than the text format. The latter is more of an interface for humans to enter program code. Program code in graph format can **(1)** be easier to process by the computer, **(2)** be enriched with any information, **(3)** link components and describe relationships.

**Example for (1):** As soon as the computer takes over tasks from the programmer, such as reformulating, expanding or correcting sections, it has to save the result back. The translation to a corresponding textual representation would be an additional step. Instead, the computer now works directly on the database.

**Example for (2):** If text files were used, guidelines, requirements or customer feedback could only be placed in comments. Since text files cannot be easily queried for interesting content like a database, clarity would suffer. Furthermore these documents would not be automatically provable/appliable in this form.

**Example for (3):** Program code in text format is one-dimensional. Links between different positions in the program code are made via identifiers. In a graph database, edges are available for this purpose. The possible number of link levels (i.e. perspectives on the project) is almost unlimited. For example, sections with specific purposes can be cataloged or properties of specific sections can be analyzed and stored.

## About me

I am a software developer from the Stuttgart area, Germany. I am currently looking for supporters to develop the idea into a product.

## Why we deserve the big prize

### Impactfulness

Many of my fellow human beings are dissatisfied, unable to realize their dreams or unable to develop their creativity. Many of them are busy making a living and surviving ever-present crises.

I dedicate my life to increasing the productivity of the global economy, to enabling progress and prosperity, to inspiring my fellow human beings, to fighting poverty and hunger and to contributing to the survival of mankind.

*According to our [Defense in Numbers](documents/Defense_in_Numbers.md), we estimate the number of direct beneficiaries of our project to be several million.*

*We estimate the positive impact on the global economy to be in the trillions of dollars.*

*According to our [Problem Statement](documents/Problem_Statement.md), we are solving a "graphy" problem. We believe this problem will become a hard limit for scientific progress and economical development within the next 100 years.*

### Innovativeness

Our approach is to store all artifacts of the software project in machine-readable form in a database, allowing the computer to "understand" the project to support the programmers and make their lives easier. Basically, the computer is given the opportunity to work on the same level as the human programmers. In other words, each human programmer is multiplied by a team of meticulous and hard-working virtual programmers.

*Storing program code in a database may seem less exciting than novel data analysis applications. However, since we are solving a "graphy" problem, it is the only solution to a very impactful problem, which is still unsolved.*

### Ambitiousness

There is still so much work waiting that one could fill a lifetime with it. First, the technology must be worked out so that it works flawlessly for all use-cases and brings benefits. Then a product must be made from it that is competitive with pretty much all existing programming tools. Last but not least, a company must be built around the product and set on a course for growth.

*According to our [Defense in Numbers](documents/Defense_in_Numbers.md), the number of entries in the database could easily reach 300 m. with more than 1.8 bn. connections between them.*

*The number of different functional features we provide could easily reach 10 000.*

### Applicability

Nevertheless, I am convinced that in the long run the pressure will increase to give us a chance. Our focus is primarily on "second-degree customers". That's what we call all the companies that use software. They obtain their software from the software manufacturers, who in turn are the customers.

*I am planning to offer our product as a seamlessly integrating solution to nearly 100% of the software producers.*

*I am also developing strategies how to target industry-specific software producers to equip nearly 100% of the economy with software that is built using our technology.*


## How we built it / Limitation of this submission

Due to my employment, I didn't know at first whether I would be able to make a submission. I hope that this submission still meets the requirements and can be considered for the competition.

It should be noted that the presented project is not a working product, in particular none of the algorithms described are included. There is another project that is supposed to demonstrate the technical implementation at some point (https://gbps-proto.gitlab.io; https://gitlab.com/gbps-proto/gbps-proto), but it is explicitly not relevant for the challenge.

For this challenge, however, I used TigerGraph to provide some sort of a presentation of the possibilities of this technology. Basically this is just a picture of our idea.

**IMPORTANT! Our submission on GitLab:** https://gitlab.com/graph-prog/million-dollar-challenge

## Contents of this Project

* [The problem statement](documents/Problem_Statement.md)
* [Our defense in numbers](documents/Defense_in_Numbers.md)
* [Use cases with queries](documents/Use_Cases_with_Queries.md)
* [The GraphStudio tarball explained](documents/GraphStudio_Tarball_Explained.txt)
* A loadable GraphStudio tarball
	* [Files included in the loadable GraphStudio tarball (schema, datasets and queries)](graphstudio_tarball)
	* [Scripts for generating datasets and packing a loadable tarball file](scripts)
	* [The loadable tarball file](graphstudio_tarball.tar.gz)

## How to load the GraphStudio tarball

1. Download the [tarball file](graphstudio_tarball.tar.gz) or download the GitLab project and run `bash scripts/pack.bash`.
2. On the GraphStudio Home page, click on "Import An Existing Solution" and upload the tarball file.
3. On the left panel, select the graph "SoftwareProject1" and click "Write Queries".
4. "Install" and "Run" the queries in following order:
	- Load_1_GeneratedVertices
	- Load_2_Vertices
	- Load_3_GeneratedEdges
	- Load_4_Edges
	- Load_5_ShowAll
5. The GraphStudio tarball is now sucessfully loaded. You can now "Install" and "Run" any of the SampleQuery_* queries for demonstration of their corresponding [use cases](documents/Use_Cases_with_Queries.md).

