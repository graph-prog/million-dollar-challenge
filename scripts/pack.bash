#bash scripts/pack.bash


mkdir /tmp/ExportedGraph

cp -f graphstudio_tarball/DBImportExport_SoftwareProject1.gsql /tmp/ExportedGraph
cp -f graphstudio_tarball/global.gsql /tmp/ExportedGraph


{
	echo 'set exit_on_error = "false"'
	echo 'CREATE QUERY Load_1_GeneratedVertices() FOR GRAPH SoftwareProject1 {'
	cat generated_vertices.gsql 
	echo 'PRINT "Load_1_GeneratedVertices() finished successfully"; '
	echo '}'
	echo 'CREATE QUERY Load_3_GeneratedEdges() FOR GRAPH SoftwareProject1 { '
	cat generated_edges.gsql 
	echo 'PRINT "Load_3_GeneratedEdges() finished successfully";'
	echo '}'
	echo 'set exit_on_error = "true"'
} >> /tmp/ExportedGraph/DBImportExport_SoftwareProject1.gsql


mkdir /tmp/graphstudio_tarball
mkdir /tmp/graphstudio_tarball/graph
mkdir /tmp/graphstudio_tarball/gui
mkdir /tmp/graphstudio_tarball/gui/user_icons

cp -f graphstudio_tarball/data.json /tmp/graphstudio_tarball/gui

pushd /tmp/ExportedGraph/
zip -r /tmp/graphstudio_tarball/graph/ExportedGraph.zip *
popd


tar czf graphstudio_tarball.tar.gz -C /tmp/graphstudio_tarball graph gui



