#python3 scripts/generate.py >generated_vertices.gsql 3>generated_edges.gsql


import os
import random
import sys


sys.setrecursionlimit(10000)


ctr = {x: 0 for x in {"Class","Declaration","Statement","Expression","Scope"}}


def addAstNode(typee,parent):
	
	name = "%s%d" % (typee.lower(),ctr[typee])
	ctr[typee] += 1
	
	os.write(1,str.encode('INSERT INTO %s VALUES ("%s",""%s);\n' % (typee,name,',""' if (typee in {"Class","Declaration"}) else '')))
	os.write(3,str.encode('INSERT INTO CONTAINS VALUES ("%s" %s,"%s" %s);\n' % (parent[1],parent[0],name,typee)))
	
	return (typee,name);


def addExpression(parent):
	
	x = addAstNode("Expression",parent)
	
	if random.random() < 0.2:
		for j in range(random.randint(1,2)):
			addExpression(x)
	
	return x


def addStatement(parent):
	
	x = addAstNode("Statement",parent)
	
	if random.random() < 0.2:
		for j in range(random.randint(1,3)):
			addExpression(x)
	
	if random.random() < 0.2:
		for j in range(random.randint(1,3)):
			addStatement(x)
	
	return x


def addDeclaration(parent):
	
	x = addAstNode("Declaration",parent)
	
	if random.random() < 0.5:
		# Method
		
		for j in range(random.randint(1,5)):
			addStatement(x)
		
	else:
		# Field
		
		addExpression(x)
	
	return x


def addClass():
	
	x = addAstNode("Class",("Classes","classes"))
	
	for j in range(random.randint(5,20)):
		addDeclaration(x)
	
	return x


for i in range(5):
	addClass()


def addScope():
	
	x = addAstNode("Scope",("Scopes","scopes"))
	
	ss = set()
	for j in range(random.randint(10,30)):
		typee = ("Class","Declaration","Statement","Expression")[random.randint(0,3)]
		name = "%s%d" % (typee.lower(),random.randint(0,ctr[typee] - 1))
		ss |= {(typee,name)}
	
	for s in ss:
		os.write(3,str.encode('INSERT INTO RELATED_TO VALUES ("%s" Scope,"%s" %s);\n' % (x[1],s[1],s[0])))
	
	return x


for i in range(8):
	addScope()



